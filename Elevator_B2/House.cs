﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;


namespace Elevator_B2
{
    /// <summary>
    /// A class in which all the objects of all other classes interact and operate. The essential algorithms for running the elevator while interacting with the people waiting on the floors of the "house"
    /// </summary>

    class House
    {
        public Elevator HouseElevator { get; private set; }

        //First dimension of list represents floor, second place in line 
        public List<List<Person>> PeopleInHouse { get; private set; }
        public List<Person> PeopleWhoFinished { get; private set; }

        public House(List<List<Person>> people)
        {
            this.PeopleInHouse = people;
            this.HouseElevator = new Elevator();
            this.PeopleWhoFinished = new List<Person>();
        }



        /// <summary>
        /// Prints out a visual representation of the elevator and house in the console
        /// </summary>
        public void PrintHouseCurrent()
        {
            Console.Clear();
            Console.WriteLine("Elevator Current Floor: " + HouseElevator.CurrentFloor + " Currently in mode: " + HouseElevator.CurrentDirMode + " Target Floor: " + HouseElevator.TargetFloor);
            int count = 0;
            foreach (List<Person> peopleAtFloor in PeopleInHouse)
            {
                Console.WriteLine("Floor: " + count);
                Console.WriteLine("___________________________________________________________________________________________________________________");
                foreach (Person person in peopleAtFloor)
                {
                    if (person.WantsToGoToFloor.Equals(-1))
                    {
                        Console.WriteLine("");
                    }
                    else
                    {
                        if(person.Direction == DirectionalModeEnum.GOINGUP) { Console.Write("U "); }
                        else { Console.Write("D "); }
                        ;
                    }
                }
                if (HouseElevator.CurrentFloor == count) { Console.WriteLine("                    Elevator: " + HouseElevator.PeopleInElevator.Count.ToString() + "/" + HouseElevator.Capacity.ToString()); }
                else { Console.WriteLine(""); }
                Console.WriteLine("___________________________________________________________________________________________________________________");
                count++;
            }
        }

        /// <summary>
        /// The essential method for running the elevator, using the "GoingUpAlgoRithm" and "GoingDownAlgorithm", checks if there are still people waiting and continues running
        /// </summary>
        public void RunAlgorithm()
        {
            bool peopleStillWaiting = true;

            while (peopleStillWaiting)
            {

                GoingUpAlgorithm();
                GoingDownAlgorithm();
                //Kollar om folk fortfarande väntar på hissen
                foreach (List<Person> FloorQueue in PeopleInHouse)
                {
                    peopleStillWaiting = false;

                    if (FloorQueue.Count != 0 || HouseElevator.PeopleInElevator.Count != 0)
                    {
                        peopleStillWaiting = true;
                        break;
                    }
                }
      
            }

            while (!peopleStillWaiting)
            {
                restart:
                HouseElevator.CurrentDirMode = DirectionalModeEnum.NEUTRAL;
                HouseElevator.TimePass(PeopleInHouse);
                Console.Clear();
                this.PrintHouseCurrent();
                Console.WriteLine(HouseElevator.SystemTime);
                foreach (List<Person> FloorQueue in PeopleInHouse)
                {
                    if (FloorQueue.Count != 0)
                    {
                        peopleStillWaiting = true;
                        RunAlgorithm();
                    }
                }
                bool newPeopleComing = true;

                if (Program.people.Count < HouseElevator.SystemTime)
                {
                    newPeopleComing = false;
                    break;
                }
                
                if (newPeopleComing)
                {
                    goto restart;
                }
            }

        }

        /// <summary>
        /// Moves the elevator upwards while picking up and dropping off passengers
        /// </summary>
        public void GoingUpAlgorithm()
        {
            HouseElevator.TargetFloor = HouseElevator.CurrentFloor + 1;
            while (HouseElevator.CurrentFloor < HouseElevator.TargetFloor)
            {
                HouseElevator.CurrentDirMode = DirectionalModeEnum.GOINGUP;
                LeaveElevator();
                foreach (List<Person> FloorQueue in PeopleInHouse)
                {
                    foreach (Person p in FloorQueue) //hissdörrarna öppnas, alla släpss
                    {
 
                        if (!HouseElevator.IsFull && p.CurrentFloor == HouseElevator.CurrentFloor)
                        {
                            if (p.Direction == DirectionalModeEnum.GOINGUP)
                            {
                                HouseElevator.EntersElevator(p);
                            }

                        }   
                    }
                    foreach (Person p in FloorQueue)
                    {
                            HouseElevator.TargetFloor = p.CurrentFloor;
                    }


                    foreach (Person p in HouseElevator.PeopleInElevator) //hissdörrarna öppnas, alla släpss
                    {
                        if(p.WantsToGoToFloor > HouseElevator.TargetFloor)
                        {
                            HouseElevator.TargetFloor = p.WantsToGoToFloor;
                        }
                        FloorQueue.Remove(p);
                    }
                    
                }
                this.PrintHouseCurrent();
                if(HouseElevator.CurrentFloor < HouseElevator.TargetFloor) {
                    HouseElevator.MoveUp(PeopleInHouse);
                }
                else { HouseElevator.TargetFloor--; }

            }
        }
        /// <summary>
        /// Moves the elevator downwards while picking up and dropping off passengers
        /// </summary>
        public void GoingDownAlgorithm()
        {

            while (HouseElevator.CurrentFloor > HouseElevator.TargetFloor)
            {
                HouseElevator.CurrentDirMode = DirectionalModeEnum.GOINGDOWN;
                LeaveElevator();
                foreach (List<Person> FloorQueue in PeopleInHouse)
                //hårdkodat antal våningar
                {
                    foreach (Person p in FloorQueue)
                    {
                        if (!HouseElevator.IsFull && p.CurrentFloor == HouseElevator.CurrentFloor)
                        {
                            if (p.Direction == DirectionalModeEnum.GOINGDOWN )
                            {         
                                    HouseElevator.EntersElevator(p);
                                
                            }
                            
                        }
                    }

                    foreach (Person p in FloorQueue)
                    {
                        if (p.Direction == DirectionalModeEnum.GOINGUP)
                        {
                            HouseElevator.TargetFloor = p.CurrentFloor;
                            break;
                        }
                    }
                    foreach (Person p in HouseElevator.PeopleInElevator)
                    {
                        FloorQueue.Remove(p);
                        if (p.WantsToGoToFloor < HouseElevator.TargetFloor)
                        {
                            HouseElevator.TargetFloor = p.WantsToGoToFloor;
                        }
                    }

                    
                }
                
                this.PrintHouseCurrent();
                HouseElevator.MoveDown(PeopleInHouse);
            }
            {

            }

        }

        /// <summary>
        /// Method for dropping for passengers
        /// </summary>
        public void LeaveElevator()
        {
            foreach (Person person in HouseElevator.PeopleInElevator.ToArray())
            {
                if (HouseElevator.CurrentFloor == person.WantsToGoToFloor)
                {
                    HouseElevator.LeavesElevator(person);
                    PeopleWhoFinished.Add(person);
                }
            }
        }
    }
}
