﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elevator_B2
{

    /// <summary>
    /// Creates an elevator with different properties to control how the elevator operates and interacts with different objects
    /// </summary>
     

    class Elevator
    {
        
        public int CurrentFloor { get;  private set; } 
        public int Capacity { get; private set; }

        public List<Person> PeopleInElevator { get; private set; }
        public List<int> ButtonsPressedForFloors { get; private set; }

        public int TargetFloor { get; set; }
        public DirectionalModeEnum CurrentDirMode { get; set; }

        public int SystemTime { get; private set; }
        public bool IsFull { get { return(PeopleInElevator.Count >= Capacity); } }


      
        public Elevator(int capacity, int startingFloor, DirectionalModeEnum startingDirection)
        {
            this.CurrentFloor = startingFloor;
            this.Capacity = capacity;

            PeopleInElevator = new List<Person>();
            ButtonsPressedForFloors = new List<int>();
            CurrentDirMode = new DirectionalModeEnum();
            CurrentDirMode = startingDirection;

            SystemTime = 0;
        }
        public Elevator() : this(10, 0, DirectionalModeEnum.NEUTRAL)
        { 
            
        }

        /// <summary>
        /// Adds a person to the elevator list
        /// </summary>
        /// <param name="person"></param> object of the class "Person"
        public void EntersElevator(Person person)
        {
            PeopleInElevator.Add(person);
        }
        /// <summary>
        /// Removes a person from the elevator list
        /// </summary>
        /// <param name="args"></param> object of the class "Person"
        public void LeavesElevator(Person person)
        {
            PeopleInElevator.Remove(person);
        }
    

        /// <summary>
        /// Increases the waiting time for each person in the house waiting for the elevator or in the elevator
        /// </summary>
        /// <param name="peopleInHouse"></param> A List of a list containing "Persons" representing the house, floors, and people in line on the floors
        public void TimePass(List<List<Person>> peopleInHouse)
        {
            foreach (List<Person> peopleOnFloor in peopleInHouse)
            {
                foreach (Person personOnFloor in peopleOnFloor)
                {
                    personOnFloor.WaitsForElevator();
                }
            }

            foreach (Person personInElevator in PeopleInElevator)
            {
                personInElevator.WaitsInElevator();
            }
            SystemTime++;
            peopleInHouse = Program.UpdateDataSet(peopleInHouse, SystemTime);
        }

        /// <summary>
        /// Moves the elevator up one floor
        /// </summary>
        /// <param name="peopleInHouse"></param> A List of a list containing "Persons" representing the house, floors, and people in line on the floors
        public void MoveUp(List<List<Person>> peopleInHouse)
        {
            TimePass(peopleInHouse);
            CurrentFloor++;

        }
        /// <summary>
        /// Moves the elevator down one floor
        /// </summary>
        /// <param name="peopleInHouse"></param> A List of a list containing "Persons" representing the house, floors, and people in line on the floors
        public void MoveDown(List<List<Person>> peopleInHouse)
        {
            TimePass(peopleInHouse);
            CurrentFloor--;

        }
    
    }
}
