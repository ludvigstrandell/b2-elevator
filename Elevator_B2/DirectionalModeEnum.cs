﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elevator_B2
{
    /// <summary>
    /// An enum signifying the direction of an object, either a persons desired direction or the elevators current direction
    /// </summary>
 

    public enum DirectionalModeEnum
    {
        GOINGUP = 1, 
        GOINGDOWN = -1,
        NEUTRAL = 0
            
    }
}
