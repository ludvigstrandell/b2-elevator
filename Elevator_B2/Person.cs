﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elevator_B2
{
    /// <summary>
    /// A class which objects represents the passengers waitng for the elevator
    /// </summary>
    class Person
    {
        public int CurrentFloor { get; private set; }

        public int WantsToGoToFloor { get; private set; }
        public int TimeWaitedForElevator { get; private set; }
        public int TimeWaitedInElevator { get; private set; }
        public int TotalTimeWaited { get { return TimeWaitedInElevator + TimeWaitedForElevator; } }

        public DirectionalModeEnum Direction{
            get
            {
                if (WantsToGoToFloor > CurrentFloor) { return DirectionalModeEnum.GOINGUP; }
                else { return DirectionalModeEnum.GOINGDOWN; };
            }
        }
        public Person(int startingFloor, int wantsToGoTofloor)
        {
            this.CurrentFloor = startingFloor;
            this.WantsToGoToFloor = wantsToGoTofloor;

            this.TimeWaitedForElevator = 0;
            this.TimeWaitedInElevator = 0;
        }

        /// <summary>
        /// Increases the persons time waited for elevator
        /// </summary>
        public void WaitsForElevator()
        {
            TimeWaitedForElevator++;
        }
        /// <summary>
        /// Increases the persons time waited in elevator
        /// </summary>

        public void WaitsInElevator()
        {
            TimeWaitedInElevator++;
        }

    }
}
