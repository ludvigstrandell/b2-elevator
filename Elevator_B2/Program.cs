﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Elevator_B2
{
    class Program
    {
        
        public static List<List<List<Person>>> people = new List<List<List<Person>>>();
        
        static void Main(string[] args)
        {
            people = ReadDataSet(@"C:\Users\nilsn\Documents\Skola\AlgoDS kurslitteratur\Projektarbete\test.txt");

            List<List<Person>> peopleT0 = people[0];

            House house = new House(peopleT0);
            house.PrintHouseCurrent();
            house.RunAlgorithm();
            Printa();

            Console.WriteLine(house.HouseElevator.SystemTime);


            ///<summary> Print is a method that takes all info about the passengers, prints it out in the console and also inserts it to a new list where each row represents a person and then exports it to a textfile.  </summary>

            void Printa()
            {
                List<Person> pp = house.PeopleWhoFinished;
                List<string> testfil = new List<string>();
                int sum = 0;
                int count = 0;
                foreach (Person p in pp)
                {

                    count++;
                    sum = sum + p.TotalTimeWaited;
                    Console.WriteLine(p.TotalTimeWaited + " Total tid för Person " + count);
                    Console.WriteLine(p.TimeWaitedForElevator + " Tid i huset för Person " + count);
                    Console.WriteLine(p.TimeWaitedInElevator + " Tid i hissen för Person " + count);

                    testfil.Add(" Person " + count + ": " + "   Total tid väntat: " + " t" + p.TotalTimeWaited + "   Total tid i huset: " + " t" + p.TimeWaitedForElevator + "   Total tid i hissen: " + " t" + p.TimeWaitedInElevator);
                }

                testfil.Add(" ");
                testfil.Add(" Antal som åkt: " + pp.Count);
                testfil.Add(" Snitttid: " + " t" + (sum / pp.Count));

                System.IO.File.WriteAllLines(@"C:\Users\nilsn\Documents\Skola\AlgoDS kurslitteratur\Projektarbete\WriteLines.txt", testfil);
            }
             
            
        }


        /// <summary> Reads the data in the csv file and adds it to a jagged lists where the first dimension represents amount of floors and the second dimension represents the line to the elevator </summary>
        /// <param name="path"></param>

        public static List<List<List<Person>>> ReadDataSet(string path)
        {
            List<List<List<Person>>> fullList = new List<List<List<Person>>>();

           
            using (var reader = new StreamReader(path))
            {
    
                while (!reader.EndOfStream)
                {
                    int floor = 0;
                    List<List<Person>> TNList = new List<List<Person>>();
                    fullList.Add(TNList);
                    for (int i = 0; i < 10; i++)
                    {
                        int wantsToGoToFloor = new int();
                        List<Person> floorList = new List<Person>();

                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        foreach (String s in values)
                        {
                            Int32.TryParse(s, out wantsToGoToFloor);
                            if (wantsToGoToFloor != -1 && wantsToGoToFloor != floor)
                            {
                                floorList.Add(new Person(floor, wantsToGoToFloor));
                            }
                        }

                        floor++;
                        TNList.Add(floorList);

                    }
                }
            }
            return fullList;
        }

        /// <summary> Checks if there are any operations left to do, if there is the dataset gets updated with the new passengers</summary>
        /// <param name="peopleTN"></param>
        /// <param name="t"></param>
        /// <returns>returns the dataset, updated or not</returns>
        public static List<List<Person>> UpdateDataSet(List<List<Person>> peopleTN, int t)
        {
            if (people.Count > t)
            {
                List<List<Person>> newPeople = people[t];
            
                for (int i = 0; i< 10; i++)
                {
                    List<Person> peopleFloor = newPeople[i];
                    foreach (Person p in peopleFloor)
                    {
                        
                            peopleTN[i].Add(p);
                    }
                }
            }
            return peopleTN;
        }


    }
}

